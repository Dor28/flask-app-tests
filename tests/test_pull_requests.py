from unittest import TestCase
from handlers.pull_requests import get_pull_requests, label_finder, label_equal
from dotenv import load_dotenv
import os
import requests
import pytest

load_dotenv()

TOKEN = os.getenv("TOKEN")
HEADERS = {'Authorization': f'Bearer {TOKEN}'}


class TestPullRequest(TestCase):
    @pytest.mark.parametrize(
    "input_value, expected", [(2, 8), (3, 4), (4, 42)])
    def test_pull_request(self):
        r = requests.get("https://api.github.com/repos/boto/boto3/pulls", headers=HEADERS)
        self.assertEqual(r.status_code, 200)
        
        expected_res = [{"num": 1, "title": "title", "link": "url"}]
        res = get_pull_requests(state="open")
        self.assertEqual(res, expected_res)
        
       
    def test_invalid_pull_request(self):
        expected_res = [{"num": 1, "title": "title", "link": "url"}]
        res_closed = get_pull_requests(state="closed")
        self.assertNotEqual(res_closed, expected_res)
        res_closed = get_pull_requests(state="bug")
        self.assertEqual(res_closed, expected_res)

    def test_invalid_label_finder(self):
        p = []
        res = label_finder(p, 2)
        self.assertFalse(res)

    def test_label_finder(self):
        p = [{"num": 1, "title": "title", "link": "url"}]
        res = label_finder(p, 1)
        self.assertTrue(res)
       
        

    def test_label_equal(self):

        res = label_equal("label", "label")
        self.assertTrue(res)
    
    def test_label_false_equal(self):
        false_res = label_equal("label","l")
        self.assertFalse(false_res)