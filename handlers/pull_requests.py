import requests
import os
from dotenv import load_dotenv


load_dotenv()

TOKEN = os.getenv("TOKEN")
HEADERS = {'Authorization': f'Bearer {TOKEN}'}


def label_equal(label, label_value):
    matching = False
    if label == label_value:
            matching = True
            return matching


def label_finder(p, label_value):
    matching = False
    for label in p["labels"]:
        matching = label_equal(label["name"], label_value)
        return matching

     
def get_pull_requests(state):
    """
    Example of return:
    [
        {"title": "Add useful stuff", "num": 56, "link": "https://github.com/boto/boto3/pull/56"},
        {"title": "Fix something", "num": 57, "link": "https://github.com/boto/boto3/pull/57"},
    ]
    """

    # Write your code here
    r = requests.get("https://api.github.com/repos/boto/boto3/pulls", headers=HEADERS)
    if r.status_code == 200:
        if state == "open":
            return [p for p in r.json() if p["state"] == "open"]   
        elif state == "closed":
            return [p for p in r.json() if p["state"] == "closed"]
        elif state == "bug":
            return [p for p in r.json() if p["state"] == "open" and label_finder(p, "bug")]
        elif state == "needs-review":
            return [p for p in r.json() if p["state"] == "open" and label_finder(p, "needs-review")]

    